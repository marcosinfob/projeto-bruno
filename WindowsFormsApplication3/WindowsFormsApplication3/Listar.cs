﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication3.DB.Base.Produto;

namespace WindowsFormsApplication3
{
    public partial class Listar : Form
    {
        public Listar()
        {
            InitializeComponent();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Campos newForm2 = new Campos();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();

            dataGridView1.AutoGenerateColumns = false;
            dataGridView1.DataSource = lista;
        }
    }
}
