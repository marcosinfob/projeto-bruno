﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication3.DB.Base.Produto;

namespace WindowsFormsApplication3
{
    public partial class Produtos : Form
    {
        public Produtos()
        {
            InitializeComponent();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {
            Campos newForm2 = new Campos();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Produto = textBox1.Text;
                dto.Preco = Convert.ToDecimal(textBox2.Text);

                ProdutoBusiness business = new ProdutoBusiness();
                business.Salvar(dto);

                MessageBox.Show("O Produto foi Salvo com sucesso.");
            }
            catch
            {
                if (textBox1 != null)
                {
                    MessageBox.Show("OBS: Não Deixe Nenhum Campo Em Branco | Use Apensa Numeros No Preço | Não Repita O Nome do Produto.");
                }
                
            }
        }
        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
