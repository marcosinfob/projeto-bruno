﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication3.DB.Base.Produto.Pedido
{
    class CompraDatabase
    {
        public int Salvar(CompraDTO dto)
        {
            string script =
                @"insert into tb_compra (produto,quantidade,valor)
	                                  values (@produto,@quantidade,@valor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("produto", dto.Produto));
            parms.Add(new MySqlParameter("quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("valor", dto.valor));
          
            

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public List<CompraDTO> Listar()
        {
            string script = "select * from tb_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<CompraDTO> lista = new List<CompraDTO>();
            while (reader.Read())
            {
                CompraDTO dto = new CompraDTO();
                dto.Produto = reader.GetString("produto");
                dto.Quantidade = reader.GetDecimal("quantidade");
                dto.valor = reader.GetDecimal("valor");
               

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
