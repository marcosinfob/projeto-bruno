﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication3.DB.Base.Produto.Estoque
{
    class EstoqueDatabase
    {
        public int Salvar(EstoqueDTO dto)
        {
            string script =
                @"insert into tb_estoque (nm_produto, nm_quantidade, nm_preco)
	                                  values (@nm_produto, @nm_quantidade, @nm_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("nm_quantidade", dto.Quantidade));
            parms.Add(new MySqlParameter("nm_preco", dto.Preco));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        
        public List<EstoqueDTO> Listar()
        {
            string script = "select * from tb_estoque";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<EstoqueDTO> lista = new List<EstoqueDTO>();
            while (reader.Read())
            {
                EstoqueDTO dto = new EstoqueDTO();
                dto.Produto = reader.GetString("nm_produto");
                dto.Quantidade = reader.GetDecimal("nm_quantidade");
                dto.Preco = reader.GetDecimal("nm_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
