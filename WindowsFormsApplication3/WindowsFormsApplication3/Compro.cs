﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication3.DB.Base.Produto.Pedido;

namespace WindowsFormsApplication3
{
    public partial class Compro : Form
    {
        public Compro()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Campos newForm2 = new Campos();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            {
                {
                    try
                    {
                        CompraDTO dto = new CompraDTO();
                        dto.Produto = txtproduto.Text;
                        dto.Quantidade = Convert.ToDecimal(txtquantidade.Text);
                        dto.valor = Convert.ToDecimal(txtvalor.Text);
                      

                        new CompraBusiness().Salvar(dto);
                      
                    }
                    catch
                    {

                        MessageBox.Show("OBS: Não Deixe Nenhum Campo Em Branco | Use Apensa Numeros No Valor e Quantidade | Não Repita O Nome do Produto.");
                    }

                }


                CompraBusiness business = new CompraBusiness();
                List<CompraDTO> lista = business.Listar();

                listar.AutoGenerateColumns = false;
                listar.DataSource = lista;
            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
