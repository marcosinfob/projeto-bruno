﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace marcos
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form3 newForm2 = new Form3();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form4 newForm2 = new Form4();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Form5 newForm2 = new Form5();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form6 newForm2 = new Form6();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form7 newForm2 = new Form7();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form8 newForm2 = new Form8();
            this.Hide();
            newForm2.ShowDialog();
        }
    }
}
