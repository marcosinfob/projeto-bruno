﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace marcos.Resources.DB.Base.Veiculo
{
    class VeiculoDatabase
    {
        public int Salvar(VeiculoDTO dto)
        {
            string script =
                @"insert into veiculo (nome,veiculo,ano,problema,placa,marca,preco,sinal)
	                                  values (@nome,@veiculo,@ano,@problema,@placa,@marca,@preco,@sinal)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("nm_preco", dto.Preco));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<VeiculoDTO> Listar()
        {
            string script = "select * from veiculo";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<VeiculoDTO> lista = new List<VeiculoDTO>();
            while (reader.Read())
            {
                VeiculoDTO dto = new VeiculoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Produto = reader.GetString("nm_produto");
                dto.Preco = reader.GetString("nm_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}


