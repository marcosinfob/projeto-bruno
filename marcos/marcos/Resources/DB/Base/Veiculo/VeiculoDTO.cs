﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace marcos.Resources.DB.Base.Veiculo
{
    class VeiculoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Veiculo { get; set; }
        public string Ano { get; set; }
        public string Problema { get; set; }
        public string Placa { get; set; }
        public string Marca { get; set; }
        public string Preco { get; set; }
        public string Sinal { get; set; }
    }
}
