﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace marcos.Resources.DB.Base.Cadastro
{
    class CadastroBusiness
    {
        public int Salvar(CadastroDTO dto)
        {
        
            CadastroDatabase db = new CadastroDatabase();
            return db.Salvar(dto);
        }

        public List<CadastroDTO> Listar()
        {
            CadastroDatabase db = new CadastroDatabase();
            return db.Listar();
        }
    }
}