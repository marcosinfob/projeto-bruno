﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace marcos.Resources.DB.Base.Cadastro
{
    class CadastroDatabase
    {
        public int Salvar(CadastroDTO dto)
        {
            string script =
                @"insert into tb_funcionario2 (nm_nome,nm_cpf,nm_nascimento,nm_endereco, nm_numero,nm_telefone,nm_email)
	                                  values (@nm_nome,@nm_cpf,@nm_nascimento,@nm_endereco,@nm_numero,@nm_telefone,@nm_email)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_pessoa", dto.Nome));
            parms.Add(new MySqlParameter("nm_cpf", dto.Cpf));       
            parms.Add(new MySqlParameter("nm_endereco", dto.Endereco));
            parms.Add(new MySqlParameter("nm_numero", dto.Numero));
            parms.Add(new MySqlParameter("nm_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nm_email", dto.Email));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<CadastroDTO> Listar()
        {
            string script = "select * from tb_funcionario2";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<CadastroDTO> lista = new List<CadastroDTO>();
            while (reader.Read())
            {
                CadastroDTO dto = new CadastroDTO();
                dto.Nome = reader.GetString("nm_nome");
                dto.Cpf = reader.GetString("nm_cpf");              
                dto.Endereco = reader.GetString("nm_endereco");
                dto.Numero = reader.GetString("nm_numero");
                dto.Telefone = reader.GetString("nm_telefone");
                dto.Email = reader.GetString("nm_email");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}

