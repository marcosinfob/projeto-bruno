﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace marcos.Resources.DB.Base.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script =
                @"insert into cadastro2 (nome,cpf,emprego,endereco,telefone)
	                                  values (@nome,@cpf,@emprego,@endereco,@telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nome", dto.Nome));
            parms.Add(new MySqlParameter("cpf", dto.Cpf));
            parms.Add(new MySqlParameter("emprego", dto.Emprego));
            parms.Add(new MySqlParameter("endereco", dto.Endereco));
            parms.Add(new MySqlParameter("telefone", dto.Telefone));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<ClienteDTO> Listar()
        {
            string script = "select * from cadastro2";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);


            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Cpf = reader.GetString("cpf");
                dto.Nome = reader.GetString("nome");
                dto.Emprego = reader.GetString("emprego");
                dto.Telefone = reader.GetString("telefone");
                dto.Endereco = reader.GetString("endereco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

    }
}
