﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace marcos.Resources.DB.Base.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {        
           ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }


        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }
    }

}