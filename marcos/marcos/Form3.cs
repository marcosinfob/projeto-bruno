﻿using marcos.Resources.DB.Base.Cadastro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace marcos
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            Form2 newForm2 = new Form2();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CadastroDTO dto = new CadastroDTO();
            dto.Nome = txtnome.Text;
            dto.Cpf = txtcpf.Text;
           
            dto.Endereco = txtendereco.Text;
            dto.Numero = txtnumero.Text;
            dto.Telefone = txttelefone.Text;
            dto.Email = txtemail.Text;

            CadastroBusiness business = new CadastroBusiness();
            business.Salvar(dto);

            MessageBox.Show("A mensagem foi enviada com sucesso.");
        }
    }
}
