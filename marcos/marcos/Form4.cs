﻿using marcos.Resources.DB.Base.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace marcos
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Form2 newForm2 = new Form2();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            ClienteDTO dto = new ClienteDTO();
            dto.Nome = nome.Text;
            dto.Cpf = txtcpf.Text;
            dto.Emprego = emprego.Text;
            dto.Endereco = endereco.Text;
            dto.Telefone = txttelefone.Text;

            ClienteBusiness business = new ClienteBusiness();
            business.Salvar(dto);

            MessageBox.Show("O Estoque foi Salvo com sucesso.");
        }
    }
}
