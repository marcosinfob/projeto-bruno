﻿using marcos.Resources.DB.Base.Cadastro;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace marcos
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            MessageBox.Show("A Tabela vai listar os dados que foram anexados na tábela.");
        }

        private void button3_Click(object sender, EventArgs e)
        {

            Form2 newForm2 = new Form2();
            this.Hide();
            newForm2.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CadastroBusiness business = new CadastroBusiness();
            List<CadastroDTO> lista = business.Listar();

            griidmarcos.AutoGenerateColumns = false;
            griidmarcos.DataSource = lista;
        }
    }
}
